// console.log("Hi there");
const form = document.getElementById('form');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordvalid = document.getElementById('passwordvalid');
const emailvalid = document.getElementById('emailvalid');
const rolevalid = document.getElementById('rolevalid');
const permvalid = document.getElementById('permvalid');
const submit = document.getElementById('submit');
const role = document.getElementsByName('roleType');
const gender = document.getElementById('gender');
const markedCheckbox = document.querySelectorAll('input[type="checkbox"]');




let validEmail = false;
let validPassword = false;
let validPerm = false;
let validRole = false;
let roleValue;


//FUNCTION FOR VALIDATING EMAIL
const validateEmail = () => {
    let regex = /^([_\-\.a-z0-9A-Z]+)@([a-zA-Z0-9])+\.([a-zA-z]){1,5}(\.\w{2,3})*$/;
    let str = email.value;

    if (regex.test(str)) {
        // console.log("Valid mail ");
        // email.classList.remove('is-invalid')
        validEmail = true;
        email.classList.remove('error');
        emailvalid.classList.remove('form-text-error');

        // console.log("valid email");
        // console.log(email.value);

    }
    else {
        validEmail = false;
        email.classList.add('error');
        emailvalid.classList.add('form-text-error');

        // console.log("Invalid email");

    }
}


//FUNCTION FOR VALIDATING PASSWORD
const validatePassword = () => {
    let regex = /^(?!\S*\s)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*.{6,}$/;
    let str = password.value;

    if (regex.test(str)) {
        validPassword = true;
        password.classList.remove('error');
        passwordvalid.classList.remove('form-text-error');

        // console.log("valid Pass");
        // console.log(password.value);


    }
    else {
        // console.log("In Valid mail ");
        validPassword = false;
        password.classList.add('error');
        passwordvalid.classList.add('form-text-error');
        // console.log("Invalid pass")
        // console.log(password.value);

    }
}

//FUNCTION FOR THE RADIO BUTTONS
const validateRadio = () => {


    if (role[0].checked || role[1].checked) {
        console.log("Role selected succesfully");
        rolevalid.classList.remove('form-text-error');
        validRole = true;
        (role[0].checked) ? roleValue = role[0].value : roleValue = role[1].value;
    }

    else {
        console.log("Please select your role");
        rolevalid.classList.add('form-text-error');
    }
}

const checkboxes = () => {
    var inputElems = document.getElementsByTagName("input"),
        count = 0;

    for (var i = 0; i < inputElems.length; i++) {
        if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
            count++;
        }

    }
    if (count < 2) {

        permvalid.classList.add('form-text-error');
    }
    else {
        permvalid.classList.remove('form-text-error');
        validPerm = true;

    }
    // console.log(count);
}

submit.addEventListener('click', (e) => {
    e.preventDefault();
    // console.log("form submitted");
    // console.log(email.value);
    validateEmail();
    validatePassword();
    validateRadio();
    checkboxes();
    if (validEmail && validPassword && validRole && validPerm) {
        // console.log("login sucess");
        let permStr = "";
        markedCheckbox.forEach((item) => {
            if (item.checked) {
                permStr += item.value;
                permStr += ',';
            }
        });
        permStr = permStr.substring(0, permStr.length - 1);
        form.style.display = "none";
        let confirmForm = document.getElementById('confirmForm');
        confirmForm.innerHTML += ` <p><strong>Email :</strong> ${email.value}</p>
                             <p><strong>Gender :</strong> ${gender.value}</p>
                             <p><strong>Role :</strong> ${roleValue}</p>
                             <p><strong>Permissions :</strong> ${permStr}</p>

                             </br>
                             </br>
                             <button>Confirm</button>
    `
    }
});
